# Robotics Assessment 2018

Please run `make build` to build the sources.

In order to run the project, use either `roslaunch assessment.launch` or `make launch`.
Running different parts can be done as follows:

# Driving

Please run `rosrun assessment driver` to initialize the driving node.
This node needs to receive a message from the planning node, which should autostart.
If it doesn't, please run `rosrun assessment planning assets/map_inflated.png`.

# Localization

Please run `rosrun assessment localization` to initialize the localization node.
The localization works only when the robot is driving around so that it can map out what the map looks like.
You should run `rosrun assessment wander` to initialize the wandering node, which simply wanders around the map to scan features with the laser scanner.

# Image processing

Please run `rosrun assessment image_processing` to initialize the image processing node.
You might want to move the robot close to the red objects in stage to see persistent mapping (all points that are red will stay in the point cloud forever).

.PHONY: build
all: build launch

TEMPLATES := $(shell find . -type f -name "*.m4")
SRC := "$(abspath .)"

DEPS_BUILD := . "/opt/ros/"*"/setup.sh"
DEPS := $(DEPS_BUILD) && . "$(abspath .)/devel/setup.sh"

build:
	# Build files from m4 templates.
	@for file in $(TEMPLATES); do \
		echo "Building template $$file..."; \
		m4 -D "PROJECT_SRC=$(SRC)" "$$file" > "$${file%.m4}"; \
	done
	# Build the project.
	$(DEPS_BUILD) && catkin_make

clean:
	# Clean all the built templates.
	@for file in $(TEMPLATES); do \
		file="$${file%.m4}"; \
		echo rm $$file; \
		if [ -f "$$file" ]; then \
			rm "$$file"; \
		fi \
	done
	# Clean the build files.
	rm -rf build devel
	rm -f vgcore.*

launch:
	$(DEPS) && roslaunch assessment.launch

/*
 * Mikko
 */

#include "ros/ros.h"
#include "std_msgs/String.h"
#include "nav_msgs/Odometry.h"
#include "visualization_msgs/Marker.h"
#include "tf/transform_broadcaster.h"

visualization_msgs::Marker goalMarker(std::vector<double> pos){
	// Display goal marker.
	visualization_msgs::Marker marker;
	marker.header.frame_id = "map";
	marker.header.stamp = ros::Time::now();

	marker.id = 0;
	marker.type = visualization_msgs::Marker::CYLINDER;
	marker.action = visualization_msgs::Marker::ADD;

	marker.pose.position.x = pos[0];
	marker.pose.position.y = pos[1];
	marker.pose.position.z = 0;

	marker.scale.x = 0.2;
	marker.scale.y = 0.2;
	marker.scale.z = 0.2;

	marker.color.a = 1;
	marker.color.r = 1;
	marker.color.g = 0;
	marker.color.b = 0;

	return marker;
}

int main(int argc, char **argv){
	ros::init(argc, argv, "goals");
	ros::NodeHandle n;

	int goals = 5;

	visualization_msgs::Marker goalMarkers [goals];
	ros::Publisher goalPubs [goals];

	for (int i = 0; i < goals; i++) {
		if (ros::param::has("/goal" + std::to_string(i))) {
			std::vector<double> goal;
			ros::param::get("/goal" + std::to_string(i), goal);

			goalPubs[i] = n.advertise<visualization_msgs::Marker>("/goal" + std::to_string(i) + "_marker", 1000);
			goalMarkers[i] = goalMarker(goal);
		} else {
			ROS_INFO("Error: Parameter not found for /goal");
			return 1;
		}
	}

	ros::Rate goal_rate(100);

	while(ros::ok()){
		for (int i = 0; i < goals; i++) {
			goalPubs[i].publish(goalMarkers[i]);
		}

		ros::spinOnce();

		goal_rate.sleep();
	}

	return 0;
}

/**
 * localization.cpp
 * One line description about this file
 * More information
 */

#include "ros/ros.h"
#include "std_msgs/ColorRGBA.h"
#include "visualization_msgs/Marker.h"
#include "geometry_msgs/Point.h"
#include "nav_msgs/OccupancyGrid.h"
#include "nav_msgs/Odometry.h"
#include "sensor_msgs/LaserScan.h"

#include <unordered_map>

#define PI 3.1415926535
#define DEBUG false

nav_msgs::OccupancyGrid map_data;

// TODO: Use confidence to tell the main robot whether to wander or drive.

typedef struct Particle {
	double x;
	double y;
	int age; // TODO: get rid of this.
	double theta;
	double probability;
} Particle;

double distance(double x1, double y1, double x2, double y2) {
	double dx = x1 - x2;
	double dy = y1 - y2;

	return std::sqrt(dx*dx + dy*dy);
}

class LocalizationCloud {
	private:
		visualization_msgs::Marker marker;

		std::list<Particle> particles = {};

	public:
		nav_msgs::Odometry last_odom;
		nav_msgs::Odometry guess_odom;
		sensor_msgs::LaserScan last_scan;

		visualization_msgs::Marker debug_marker;

		visualization_msgs::Marker get_markers() {
			// How many markers to display at max?
			// If this is 0, display all of them.
			int display_amount_max = particles.size()*1.0; // Display 100%

			marker.points.clear();
			marker.colors.clear();

			marker.header.stamp = ros::Time::now();

			for (Particle particle : particles) {
				geometry_msgs::Point point;
				point.x = particle.x;
				point.y = particle.y;
				marker.points.push_back(point);

				// TODO: Colour individuals using the probability.
				int age = particle.age;
				std_msgs::ColorRGBA color;
				color.a = 1;
				color.r = 1;
				color.g = 1;
				color.b = 0;
				marker.colors.push_back(color);
				if (--display_amount_max==0) break;
			}

			return marker;
		}

		void createParticles(int particle_count) {
			// Create our particles based on the occupancy grid.
			// The robot cannot be inside an obstacle, so take a position that
			// is unoccupied.

			// We create an uniform distribution at first.
			for (int i = 0; i < map_data.data.size(); i++) {
				if (map_data.data[i] > 0) continue;
				distribution_list.push_back(i);
			}

			for (int i = 0; i < particle_count; i++) {
				Particle particle;

				shuffleParameters(&particle);

				particles.push_back(particle);
			}
		}

		// Random number 0..1
		double random01() {
			return ((double)rand() / ((double)RAND_MAX));
		}

		// Random number -1..1.
		double random() {
			return (random01() - 0.5) * 2.0;
		}

		std::deque<int> distribution_list;

		void shuffleParameters(Particle * particle) {
			// TODO: This method assumes the map is sparse.
			// TODO: We should also in some cases acknowledge the odom reading.
			// e.g. if odom can be trusted (not a kidnapped robot problem), then
			// we can init our particles closer to the odom pos.

#if !DEBUG
			// Get a random unoccupied point from the map data.
			int index = distribution_list[random01() * distribution_list.size()];
			// int index = random01()*map_data.data.size();

			std::vector<double> xy = data_to_xy(index);
			particle->x = xy[0];
			particle->y = xy[1];

			// Random between -pi and pi
			// See: https://en.wikipedia.org/wiki/Atan2#/media/File:Atan2atan.png
			particle->theta = random() * PI;
#else
			// DEBUG: Get points near the odom point.
			std::vector<double> xy = {
				last_odom.pose.pose.position.x,
				last_odom.pose.pose.position.y
			};

			double error = 0.022;

			particle->x = xy[0] + (random01()-0.5)*error;
			particle->y = xy[1] + (random01()-0.5)*error;
			// particle->theta = quaternion_to_theta(last_odom.pose.pose) + (random01()-0.5)*error;
			particle->theta = random() * PI;
#endif

			particle->age = 0;

			particle->probability = -1;
		}

		std::vector<double> theta_to_quaternion(double theta) {
			// Abbreviations for the various angular functions
			double cy = cos(theta * 0.5);
			double sy = sin(theta * 0.5);
			double cr = cos(0 * 0.5);
			double sr = sin(0 * 0.5);
			double cp = cr;
			double sp = sr;

			std::vector<double> quat = { 0, 0, 0, 0 };
			quat[0] = cy * cr * cp + sy * sr * sp;
			quat[1] = cy * sr * cp - sy * cr * sp;
			quat[2] = cy * cr * sp + sy * sr * cp;
			quat[3] = sy * cr * cp - cy * sr * sp;
			return quat;
		}

		// Get euler rotation (z) from quaternion
		double quaternion_to_theta(geometry_msgs::Pose p) {
			double siny_cosp = 2.0 * (p.orientation.w * p.orientation.z + p.orientation.x * p.orientation.y);
			double cosy_cosp = 1.0 - 2.0 * (p.orientation.y * p.orientation.y + p.orientation.z * p.orientation.z);
			return atan2(siny_cosp, cosy_cosp);
		}

		// Convert data index to the map x,y coords.
		std::vector<double> data_to_xy(int index) {
			int width = map_data.info.width;

			// Index is in row-major order, so getting x and y in map data coords is easy.
			int x = index % width;
			int y = index / width;

			std::vector<double> xy = { 0, 0 };

			xy[0] = ((double)x) * map_data.info.resolution + map_data.info.origin.position.x;
			xy[1] = ((double)y) * map_data.info.resolution + map_data.info.origin.position.y;

			return xy;
		}

		// Convert map x,y coords to data index.
		int xy_to_data(double x, double y) {
			int px = (x - map_data.info.origin.position.x) / map_data.info.resolution;
			int py = (y - map_data.info.origin.position.y) / map_data.info.resolution;

			int width = map_data.info.width;
			int index = py * width + px;
			return index;
		}

		bool occupied(double x, double y) {
			int index = xy_to_data(x, y);
			return map_data.data[index] > 0;
		}

		// There has been an update to the odometry.
		void odom_update(nav_msgs::Odometry odom) {
			// Update our particles based on how much we think we have progressed.

			double speed_tune = 2.0;

			double theta = quaternion_to_theta(odom.pose.pose);
			double last_theta = quaternion_to_theta(last_odom.pose.pose);

			// We first want to get the approximate distance travelled from the distance between the two odoms.
			double dx = odom.pose.pose.position.x - last_odom.pose.pose.position.x;
			double dy = odom.pose.pose.position.y - last_odom.pose.pose.position.y;
			double dt = theta - last_theta;

			dt *= speed_tune;

			// Distance from radius of circle.
			// TODO: average between last_odom and odom theta for precision?
			double distance = dx * cos(theta) + dy * sin(theta);

			distance *= speed_tune;

			for (Particle & particle : particles) {
				// Update the particle position based on the dist travelled.
				// TODO: Should there be a random error to this?
				particle.x += cos(particle.theta) * distance;
				particle.y += sin(particle.theta) * distance;
				particle.theta += dt;
			}
			last_odom = odom;
		}

		std::vector<int> ros_to_xy(double x, double y) {
			int px = (x - map_data.info.origin.position.x) / map_data.info.resolution;
			int py = (y - map_data.info.origin.position.y) / map_data.info.resolution;
			std::vector<int> xy = { px, py };
			return xy;
		}

		bool outside_of_map(double x, double y) {
			std::vector<int> xy = ros_to_xy(x, y);

			int width = map_data.info.width, height = map_data.info.height;

			return (xy[0] < 0 || xy[0] > width || xy[1] < 0 || xy[1] > height);
		}

		// Update to the laser scans.
		void scan_update(sensor_msgs::LaserScan scan) {
			// Update our probabilities.
			// Based on the scan we got, how probable would each individual particle be?
			double ranges = scan.ranges.size();

			int partid = 0;
			for (Particle & particle : particles) {
				// We want to discard particles that are outside of the map.
				if (outside_of_map(particle.x, particle.y)) {
					shuffleParameters(&particle);
					continue;
				}

				// Or inside objects.
				if (occupied(particle.x, particle.y)) {
					shuffleParameters(&particle);
					continue;
				}

				// Get a fake 'scan' that corresponds to how our real robot would.
				double angle = scan.angle_min;
				double probability = 0.0;

				for (int i = 0; i < ranges; i++) {
					// TODO: In order to improve performance, we could probably not check all scans and instead check every 2nd, 3rd etc.
					// This will probably impact accuracy though, but we can have more particles.
					//
					// For every scan, we want to calculate the fake range,
					// and compare it to the real range.
					// We set the min and max ranges for clamping,
					// otherwise we will get weird probabilities if there are no obstacles.
					// The fake scan would return 5.0, and the real one 3.0, just because 3.0 is the max range.

					double real_range = scan.ranges[i];
					double fake_scan = particle_fake_scan(particle, angle, real_range, scan.range_max, scan.range_min);
					probability += fake_scan/(float)ranges;

					// double range = particle_scan_range(particle, angle, scan.range_min, scan.range_max);

					if (i == 0) {
						// std::cout << real_range << "\n";
						// if (fake_scan) {
						// 	std::cout << "YES IT IS!\n";
						// } else {
						// 	std::cout << "NEIN!\n";
						// }
					}

					// Get the probability that this was the range
					/*
						 double range_diff_norm = (abs(range - real_range) / scan.range_max);
						 probability += (1.0 - range_diff_norm*range_diff_norm*range_diff_norm)/ranges;
						 */
					angle += scan.angle_increment;
				}

				particle.probability = probability;

				partid++;
			}

			last_scan = scan;
		}

		double particle_fake_scan(Particle particle, double angle, double range, double min = 0.0, double max = 3.0) {
			// First, check if we are 'close' to any boundaries.
			double epsilon = 0.4;
			if (abs(range - min) < epsilon) return 1.0;
			if (abs(range - max) < epsilon) return 1.0;

			double x = particle.x;
			double y = particle.y;
			double theta = particle.theta + angle;

			// So there should be an obstacle 'near' our range.
			// The range sensor mainly overshoots - if this changes,
			// you need to adjust the parameters accordingly.
			// The easiest way to do this is to spawn only one particle and
			// draw the checks as points, tracking how probable the particle
			// gets.
			int checks = 4;

			for (int i = -1; i < checks; i++) {
				// Check around this range as there are errors.
				double r = range + i * 0.122;
				double nx = x + cos(theta) * r, ny = y + sin(theta) * r;

				/*
					 geometry_msgs::Point dp;
					 dp.x = nx;
					 dp.y = ny;
					 debug_marker.points.push_back(dp);
					 */

				if (outside_of_map(nx, ny)) {
					// Readings way outside of the map should return 0.
					// However, hitting the 'edge' should return 1.
					std::vector<int> xy = ros_to_xy(nx, ny);

					double px = xy[0];
					double py = xy[1];

					double near_enough = 10;
					if (abs(px) < near_enough || abs(px - map_data.info.width) < near_enough) return 1.0;
					if (abs(py) < near_enough || abs(py - map_data.info.height) < near_enough) return 1.0;

					return 0.0;
				}

				// There is an obstacle!
				if (occupied(nx, ny)) return 1.0;

			}
			return 0.0;
		}

		void iteration() {
			// Redistribute particles that have the lowest probability.
			// The lowest n% of particles
			double clone_cutoff = 0.01; // Clone the lowest 1%.
			double prob_cutoff = 0.2; // Shuffle 5%-20%.

			std::deque<Particle*> sorted_particles = {};

			// First, update our distribution grid.
			// distribution_list.clear();

			// Calc this here as it's cheaper.
			double angle = random() * PI;

			for (Particle & particle : particles) {
				particle.age++;
				sorted_particles.push_back(&particle);

				int index = xy_to_data(particle.x, particle.y);
				// distribution_list.push_back(index);
			}

			// Allow for some randomness so we can recover from errors.
			// for (int i = 0; i < particles.size() * rand_cutoff; i++) {
			// 	// distribution_list.push_back(random01()*map_data.data.size());
			// }

			// Sort our particles based on their probability, lowest first.
			std::sort(sorted_particles.begin(), sorted_particles.end(), [](Particle * a, Particle * b){
					return a->probability < b->probability;
					});

			// std::cout << "s:" << total << "\n";
			// std::cout << "f:" << sorted_particles.front()->probability << "\n";

			// Get the prediction from the most probable positions.
			// Clone these positions.
			debug_marker.points.clear();

			int count = sorted_particles.size();
			for (int i = 0; i < count * clone_cutoff; i++) {
				Particle * good_particle = sorted_particles[count - i - 1];
				Particle * bad_particle = sorted_particles[i];

				double randangle = random() * PI;
				double error = 0.1;

				double vx = cos(randangle) * error;
				double vy = sin(randangle) * error;

				double nx = good_particle->x;
				double ny = good_particle->y;

				bad_particle->x = nx;
				bad_particle->y = ny;
				bad_particle->theta = good_particle->theta + randangle * error;

				geometry_msgs::Point bp;

				bp.x = nx;
				bp.y = ny;

				//debug_marker.points.push_back(bp);
			}

			// Then redistribute.
			for (int i = count * clone_cutoff; i < count * prob_cutoff; i++) {
				Particle * shuf_particle = sorted_particles[i];
				shuffleParameters(shuf_particle);
			}

			// Finally, calc and publish our estimated pose.
			double calc_perc = 0.01; // Use only the top 2% of particles.

			// Calculate the averages.
			double avg_x = 0,
						 avg_y = 0,
						 avg_theta = 0;

			int calc_count = count * calc_perc;

			{
				for (int i = 0; i < calc_count; i++) {
					avg_x += sorted_particles[i]->x;
					avg_y += sorted_particles[i]->y;
					avg_theta += sorted_particles[i]->theta;
				}
				avg_x /= calc_count;
				avg_y /= calc_count;
				avg_theta /= calc_count;
			}

			// Calculate the standard deviations.
			double std_x = 0,
						 std_y = 0,
						 std_theta = 0;

			double previous_x = guess_odom.pose.pose.position.x,
						 previous_y = guess_odom.pose.pose.position.y,
						 previous_theta = quaternion_to_theta(guess_odom.pose.pose);

			double final_x = 0,
						 final_y = 0,
						 final_t = 0;

			// Remove outliers.
			// This is done by assuming that the robot does not teleport around;
			// in essence checking the standard deviation of the previous best guess and
			// the next particle location. If they differ more than a cutoff amount,
			// we know that this particle is probably not where we currently are.
			//
			// If the initial guess is wrong, it will take longer for the robot to
			// re-localize itself, but this will eventually happen when all the particles
			// have died and there is nothing to average from, in which case we look at
			// the first particle to be our best guess for that single frame.
			{
				double stdev_cutoff = 0.2;

				int skips = 0;
				for (int i = 0; i < calc_count; i++) {
					double dx = sorted_particles[i]->x - previous_x,
								 dy = sorted_particles[i]->y - previous_y, 
								 dt = sorted_particles[i]->theta - previous_theta;

					if (std::sqrt(dx * dx) > stdev_cutoff || std::sqrt(dy * dy) > stdev_cutoff) {
						skips++;
						continue;
					}

					final_x += sorted_particles[i]->x;
					final_y += sorted_particles[i]->y;
					final_t += sorted_particles[i]->theta;

					std_x += dx * dx;
					std_y += dy * dy;
					std_theta += dt * dt;
				}

				final_x /= calc_count-skips;
				final_y /= calc_count-skips;
				final_t /= calc_count-skips;

				std_x = std::sqrt( std_x / (calc_count-skips) );
				std_y = std::sqrt( std_y / (calc_count-skips) );
				std_theta = std::sqrt( std_theta / (calc_count-skips) );
			}

			std::cout << "sx: " << std_x << " sy: " << std_y << " st: " << std_theta << std::endl;
			double guess_x = final_x,
						 guess_y = final_y,
						 guess_theta = final_t;

			// We use the most likely particle in case there is nothing to average from.
			if (isnan(guess_x))
				guess_x = sorted_particles[0]->x;
			if (isnan(guess_y))
				guess_y = sorted_particles[0]->y;
			if (isnan(guess_theta))
				guess_theta = sorted_particles[0]->theta;

			guess_odom.pose.pose.position.x = guess_x;
			guess_odom.pose.pose.position.y = guess_y;

			std::vector<double> quat = theta_to_quaternion(guess_theta);
			guess_odom.pose.pose.orientation.w = quat[0];
			guess_odom.pose.pose.orientation.x = quat[1];
			guess_odom.pose.pose.orientation.y = quat[2];
			guess_odom.pose.pose.orientation.z = quat[3];
		};

		LocalizationCloud(){
			// Cloud viz marker
			marker.header.frame_id = "map";

			marker.type = visualization_msgs::Marker::POINTS;
			marker.action = visualization_msgs::Marker::ADD;

			marker.scale.x = 0.122;
			marker.scale.y = 0.122;
			marker.scale.z = 0;

			marker.pose.position.x =
				marker.pose.position.y =
				marker.pose.position.z = 0;

			marker.color.a =
				marker.color.r =
				marker.color.g = 1;
			marker.color.b = 0;

			// Debug marker
			debug_marker.header.frame_id = "map";

			debug_marker.type = visualization_msgs::Marker::POINTS;
			debug_marker.action = visualization_msgs::Marker::ADD;

			debug_marker.scale.x = 0.2;
			debug_marker.scale.y = 0.2;
			debug_marker.scale.z = 0.2;

			debug_marker.color.a = 1;
			debug_marker.color.r = 1;
			debug_marker.color.g = 0;
			debug_marker.color.b = 1;

			// Odom
			guess_odom.header.frame_id = "map";

			guess_odom.pose.pose.position.x = 0;
			guess_odom.pose.pose.position.y = 0;
		}
};

LocalizationCloud localization_cloud;

void odomCallback(nav_msgs::Odometry odom) {
	localization_cloud.odom_update(odom);
}

void scanCallback(sensor_msgs::LaserScan scan) {
	localization_cloud.scan_update(scan);
}

int main(int argc, char ** argv){
	ros::init(argc, argv, "localization");
	ros::NodeHandle n;

	// Initialize our random seed.
	srand(time(NULL));

	// Wait for map info.
	// We assume this does not change throughout the duration the localizer runs.
	map_data = *ros::topic::waitForMessage<nav_msgs::OccupancyGrid>("/map");

	std::cout << "LC: s:" << map_data.data.size() << "w:" << map_data.info.width << "h:" << map_data.info.height << "\n";

	localization_cloud.createParticles(1022);

	// Localization cloud marker publisher.
	ros::Publisher localization_cloud_pub = n.advertise<visualization_msgs::Marker>("/localization_cloud", 100);

	// Localization guess publisher.
	ros::Publisher localization_pose = n.advertise<nav_msgs::Odometry>("/localization_odom", 100);

	// Debug marker.
	ros::Publisher localization_cloud_single_pub = n.advertise<visualization_msgs::Marker>("/localization_cloud_single_pub", 100);

	// Subscribe to odom to get relative movement.
	ros::Subscriber odom = n.subscribe("/odom_pose", 100, odomCallback);
	// Subscribe to base scan to get laser scans for localization.
	ros::Subscriber noisy_base_scan = n.subscribe("/noisy_base_scan", 100, scanCallback);

	localization_cloud.last_odom = *ros::topic::waitForMessage<nav_msgs::Odometry>("/odom_pose");
	localization_cloud.last_scan = *ros::topic::waitForMessage<sensor_msgs::LaserScan>("/noisy_base_scan");

	ros::Rate goal_rate(30);
	while(ros::ok()){
		localization_cloud.iteration();
		localization_cloud_pub.publish(localization_cloud.get_markers());

		localization_pose.publish(localization_cloud.guess_odom);

		localization_cloud_single_pub.publish(localization_cloud.debug_marker);
		localization_cloud.debug_marker.points.clear();

		ros::spinOnce();

		goal_rate.sleep();
	}

	return 0;
}

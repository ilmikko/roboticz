/*
 * Mikko
 */

#include "ros/ros.h"
#include "std_msgs/String.h"
#include "nav_msgs/Odometry.h"
#include "visualization_msgs/Marker.h"
#include "tf/transform_broadcaster.h"

ros::Publisher real_robot_pose, real_robot, odom_pose;

void publishTransform(nav_msgs::Odometry odom){
	odom.header.frame_id = "map";
	real_robot_pose.publish(odom);

	// Display robot marker.
	visualization_msgs::Marker marker;
	marker.header.frame_id = odom.header.frame_id;
	marker.header.stamp = odom.header.stamp;

	marker.id = 0;
	marker.type = visualization_msgs::Marker::CUBE;
	marker.action = visualization_msgs::Marker::ADD;

	//marker.pose.position.x = odom.pose.pose.position.x;
	//marker.pose.position.y = odom.pose.pose.position.y;
	//marker.pose.position.z = odom.pose.pose.position.z;

	marker.pose.position = odom.pose.pose.position;
	marker.pose.orientation = odom.pose.pose.orientation;

	marker.scale.x = 0.2;
	marker.scale.y = 0.2;
	marker.scale.z = 0.2;

	marker.color.a = 1;
	marker.color.r = 0;
	marker.color.g = 1;
	marker.color.b = 0;

	real_robot.publish(marker);
}

void bpgtCallback(nav_msgs::Odometry msg){
	msg.header.frame_id = "map";
	publishTransform(msg);
}

void odomCallback(nav_msgs::Odometry odom) {
	odom.header.frame_id = "map";
	odom_pose.publish(odom);
}

int main(int argc, char **argv){
	ros::init(argc, argv, "assessment");
	ros::NodeHandle n;

	ros::Subscriber bpgt_sub = n.subscribe("/base_pose_ground_truth", 10, bpgtCallback);
	ros::Subscriber odom_sub = n.subscribe("/odom", 10, odomCallback);

	// Real pose for robot.
	real_robot_pose = n.advertise<nav_msgs::Odometry>("/real_robot_pose", 10);
	// Robot marker for real robot pose.
	real_robot = n.advertise<visualization_msgs::Marker>("/real_robot", 10);

	// Info from the odometry.
	odom_pose = n.advertise<nav_msgs::Odometry>("/odom_pose", 10);

	ros::spin();

	return 0;
}

/**
 * wander.cpp
 * Generic wandering node.
 * This is to test if localization is working.
 */

#include "ros/ros.h"
#include <cstdio>
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>
#include <nav_msgs/Odometry.h>

#define PI 3.1415926535

class Driver {
	private:

		double goal_skip_radius = 0.1;

		double distance(std::vector<double> goal) {
			double x = pose.pose.pose.position.x - goal[0];
			double y = pose.pose.pose.position.y - goal[1];
			return sqrt(x*x + y*y);
		}

		double linear_vel(std::vector<double> goal) {
			return 1.22 * distance(goal);
		}

		double angular_vel(std::vector<double> goal) {
			// https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
			double siny_cosp = 2.0 * (pose.pose.pose.orientation.w * pose.pose.pose.orientation.z + pose.pose.pose.orientation.x * pose.pose.pose.orientation.y);
			double cosy_cosp = 1.0 - 2.0 * (pose.pose.pose.orientation.y * pose.pose.pose.orientation.y + pose.pose.pose.orientation.z * pose.pose.pose.orientation.z);
			double theta = atan2(siny_cosp, cosy_cosp);

			double ang = angle(goal);
			std::cout << "a:" << ang << "t:" << theta << "z:" << pose.pose.pose.orientation.z << "w:" << pose.pose.pose.orientation.w << "\n";
			return 22.22 * (angle(goal) - theta);
		}

		double angle(std::vector<double> goal) {
			return atan2(goal[1] - pose.pose.pose.position.y, goal[0] - pose.pose.pose.position.x);
		}

	public:

		nav_msgs::Odometry pose;
		sensor_msgs::LaserScan scan;
		geometry_msgs::Twist twist;

		// Returns true if we have reached the goal.
		bool drive_to(std::vector<double> goal) {
			double epsilon = 0.01;

			if (distance(goal) < epsilon) {
				std::cout << "Yay I'm close enough.\n";
				stop();
				return true;
			}

			twist.linear.x = linear_vel(goal);
			twist.angular.z = angular_vel(goal);
			return false;
		}

		void scan_update(sensor_msgs::LaserScan _scan) {
			scan = _scan;
		}

		void wander() {
			// Check our sensor readings.
			// We try to find the most free space in our sensor readings.

			double turn_speed = 0.5;
			double drive_speed = 2.0;

			// Angle: try to find the most free space.
			double new_angle = 0.0; // 0.0 is straight
			// Velocity: the closer any object is, the slower we want to go.
			double new_vel = 1.0;

			double angle = scan.angle_min;

			for (int i = 0; i < scan.ranges.size(); i++) {
				double p = scan.ranges[i]/scan.range_max-scan.range_min;
				new_angle += p * angle;
				if (p < new_vel) { new_vel = p; }
				angle += scan.angle_increment;
			}

			twist.angular.z = new_angle * turn_speed;
			twist.linear.x = new_vel * drive_speed;
		}

		double find_angle(double angle_want) {
			double ranges = scan.ranges.size();

			if (angle_want < scan.angle_min || angle_want > scan.angle_max) {
				std::cout << "min: " << scan.angle_min << "max: " << scan.angle_max << "\n";
				throw std::runtime_error("Angle is not in range.");
			}

			double angle = scan.angle_min;

			for (int i = 0; i < ranges; i++) {
				if (angle < angle_want && angle_want < angle + scan.angle_increment) {
					return scan.ranges[i];
				}
				angle += scan.angle_increment;
			}

			throw std::runtime_error("Wrong.");
		}

		void stop() {
			twist.angular.z = 0;
			twist.linear.x = 0;
		}

		Driver() {
			twist.linear.x = twist.linear.y = twist.linear.z = 0;
			twist.angular.x = twist.angular.y = twist.angular.z = 0;
		}
};

Driver d;

void scan_callback(sensor_msgs::LaserScan scan) {
	d.scan_update(scan);
}

int main(int argc, char ** argv){
	ros::init(argc, argv, "wander");

	ros::NodeHandle n;

	ros::Publisher pub = n.advertise<geometry_msgs::Twist>("/cmd_vel", 1000);
	ros::Subscriber sub = n.subscribe("/noisy_base_scan", 1000, scan_callback);
	ros::Rate rate(15);

	// Wait until we have sensor readings.
	scan_callback(*ros::topic::waitForMessage<sensor_msgs::LaserScan>("/noisy_base_scan"));

	while(ros::ok()) {
		d.wander();
		pub.publish(d.twist);

		rate.sleep();
		ros::spinOnce();
	}

	return 0;
}


/**
 * image_processing.cpp
 * One line description about this file
 * More information
 */

#include "ros/ros.h"
// #include <cstdio>
#include "sensor_msgs/Image.h"
#include "sensor_msgs/CameraInfo.h"
// #include "sensor_msgs/PointCloud2.h"

#include "nav_msgs/Odometry.h"

#include <pcl_ros/point_cloud.h>
// #include <pcl/point_types.h>
// #include <pcl_conversions/pcl_conversions.h>
// #include <pcl/filters/statistical_outlier_removal.h>

#include "cv_bridge/cv_bridge.h"
// #include "sensor_msgs/image_encodings.h"


// #include "<tf/Matrix3x3.h>"
#include "tf/transform_broadcaster.h"
#include "tf/LinearMath/Matrix3x3.h"
#include "tf/LinearMath/Vector3.h"

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"

#define PI 3.141592653589793

typedef pcl::PointCloud<pcl::PointXYZRGB> PointCloud;

PointCloud::Ptr cloud_msg (new PointCloud);

// Contains a snapshot of the points we see at current frame.
std::list<pcl::PointXYZRGB> points;

// Contains the points that make up the object we want to map.
PointCloud::Ptr object (new PointCloud);

nav_msgs::Odometry camera_odom;

sensor_msgs::CameraInfo camera_info;

// Matrix for camera transform.
tf::Matrix3x3 camera_mat;

cv_bridge::CvImagePtr color_ptr;

void camera_callback(const sensor_msgs::ImageConstPtr& simage) {
	// std::cout << "En: " << simage->encoding << std::endl;

	try {
		color_ptr = cv_bridge::toCvCopy(simage, simage->encoding);
	}
	catch(cv_bridge::Exception& e) {
		std::cerr << "cv_bridge exception: " << e.what();
		return;
	}

	cv::imshow("image window", color_ptr->image);
	cv::waitKey(3);
}

void depth_callback(const sensor_msgs::ImageConstPtr& simage) {
	cv_bridge::CvImagePtr cv_ptr;
	try {
		cv_ptr = cv_bridge::toCvCopy(simage, simage->encoding);
	}
	catch(cv_bridge::Exception& e) {
		std::cerr << "cv_bridge exception: " << e.what();
		return;
	}

	double minVal, maxVal;
	//find minimum and maximum intensities
	minMaxLoc(cv_ptr->image, &minVal, &maxVal);

	// TODO: aren't these just 0 and 1?
	// maxVal = std::min(1.0, maxVal);

	minVal = 0.0;
	maxVal = 1.0;

	// std::cout << "min: " << minVal << " max: " << maxVal << std::endl;

	cv::Mat img;
	cv_ptr->image.convertTo(img, CV_8U, 255.0/(maxVal - minVal), -minVal * 255.0/(maxVal - minVal));

	cv::imshow("depth window", img);
	cv::waitKey(3);

	// My poor machine
	int skip = 10;

	// We account for the camera coordinate being above ground
	double camera_x = camera_odom.pose.pose.position.x,
				 camera_y = camera_odom.pose.pose.position.y,
				 camera_z = camera_odom.pose.pose.position.z + 0.15;

	std::vector<double> camera_pos = { camera_x, camera_y, camera_z };

	points.clear();
	for (int v = 0; v < img.rows; v+=skip) {
		for (int u = 0; u < img.cols; u+=skip) {
			// Get depth info.
			float depth = ((float)img.at<uchar>(v, u)) / 255.0;
			// std::cout << "C: " << color << std::endl;

			// Ignore out of range entries.
			float epsilon = 0.001;
			if (abs(depth-1.0) < epsilon) continue;

			// Camera pixel coordinates
			std::vector<int> uv = { u, v };

			// Inverse focal lengths
			float invfocallengthx = 1.f / camera_info.K[0];
			float invfocallengthy = 1.f / camera_info.K[4];

			// Principal point
			float cx = camera_info.K[2];
			float cy = camera_info.K[5];

			tf::Quaternion robot_rot(
					camera_odom.pose.pose.orientation.x,
					camera_odom.pose.pose.orientation.y,
					camera_odom.pose.pose.orientation.z,
					camera_odom.pose.pose.orientation.w
					);

			tf::Quaternion camera_rot;
			camera_rot.setEuler(PI/2.0, 0, -PI/2.0);

			robot_rot *= camera_rot;

			tf::Matrix3x3 m(robot_rot);

			// Include depth in calculations
			tf::Vector3 clipSpacePosition(uv[0], uv[1], depth);

			// Inverse camera matrix
			tf::Vector3 viewSpacePosition = camera_mat.inverse() * clipSpacePosition;

			tf::Vector3 xyz = viewSpacePosition;

			float dist = depth;
			float x = (uv[0] - cx) * dist * invfocallengthx;
			float y = (uv[1] - cy) * dist * invfocallengthy;
			float z = dist;

			xyz = m * tf::Vector3(x,y,z);

			xyz[0] += camera_x;
			xyz[1] += camera_y;
			xyz[2] += camera_z;

			// tf::Vector3 v(m.tdotx(), m.tdoty(), m.tdotz());

			// Get colour info.
			uint32_t color = color_ptr->image.at<uint32_t>(v, u);
			uint8_t b = (color >> 16) & 0x0000ff;
			uint8_t g = (color >> 8)  & 0x0000ff;
			uint8_t r = (color)       & 0x0000ff;

			pcl::PointXYZRGB point = pcl::PointXYZRGB(r, g, b);

			point.x = xyz[0];
			point.y = xyz[1];
			point.z = xyz[2];

			points.push_back(point);

			// Do not do this for all of the pixels
			if (u % (skip * 10) == 0 || v % (skip * 10) == 0) {
				// If the point is red enough
				if (r > 250 && g < 2 && b < 2) {
					// Put into our persistent cloud.
					object->width++;
					object->points.push_back(point);
				}
			}
		}
	}
}

void camera_pos_callback(nav_msgs::Odometry msg){
	camera_odom = msg;
}

int main(int argc, char ** argv) {
	ros::init(argc, argv, "image_processing");

	ros::NodeHandle n;

	cloud_msg->height = 1;
	cloud_msg->width = 0;
	cloud_msg->header.frame_id = "map";

	object->height = 1;
	object->width = 0;
	object->header.frame_id = "map";

	ros::Subscriber image_sub = n.subscribe("/image", 1000, camera_callback);
	ros::Subscriber depth_sub = n.subscribe("/depth", 1000, depth_callback);

	// Camera info for matrix.
	std::cout << "Waiting for camera info...";
	camera_info = *ros::topic::waitForMessage<sensor_msgs::CameraInfo>("/camera_info");
	std::cout << "Done!" << std::endl;

	camera_mat = tf::Matrix3x3(
			camera_info.K[0], camera_info.K[1], camera_info.K[2],
			camera_info.K[3], camera_info.K[4], camera_info.K[5],
			camera_info.K[6], camera_info.K[7], camera_info.K[8]
			);

	ros::Subscriber bpgt = n.subscribe("/base_pose_ground_truth", 1000, camera_pos_callback);

	ros::Publisher cloud_pub = n.advertise<PointCloud>("/vision_cloud", 1000);
	ros::Publisher object_pub = n.advertise<PointCloud>("/vision_object", 1000);

	ros::Rate rate(50);

	std::cout << "I'll try spinning, that is a good trick.\n";

	double angle = 0.0;

	while(ros::ok()) {
		rate.sleep();

		int points_size = points.size();

		// Create and publish the point cloud

		cloud_msg->width = points_size;

		cloud_msg->points.clear();
		for (pcl::PointXYZRGB point : points) {
			cloud_msg->points.push_back(point);
		}

		cloud_pub.publish(cloud_msg);
		object_pub.publish(object);

		ros::spinOnce();
	}

	return 0;
}

#include "ros/ros.h"
#include "std_msgs/String.h"

#include <sstream>

bool ros_ok(ros::NodeHandle n){
	// Print out rosdistro to check if core is still running.
	std::string rosdistro;
	return n.getParam("/rosversion", rosdistro);
}

int main(int argc, char **argv){
	ros::init(argc, argv, "assessment");
	ros::NodeHandle n;

	ros::Publisher chatter_pub = n.advertise<std_msgs::String>("/chatter", 1000);

	ros::Rate loop_rate(10);

	int count = 0;
	while(ros_ok(n)){
		std_msgs::String msg;

		std::stringstream ss;
		ss << "hello world " << count;
		msg.data = ss.str();
		ROS_INFO("%s", msg.data.c_str());

		chatter_pub.publish(msg);

		ros::spinOnce();

		loop_rate.sleep();
		++count;
	}

	return 0;
}

/**
 * driver.cpp
 * This ROS node is responsible for driving the robot to the goal.
 */

#include "ros/ros.h"
#include <cstdio>
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>
#include <nav_msgs/Odometry.h>

#define PI 3.141592653589

class Driver {
	private:

		double goal_skip_radius_max = 0.522;
		double goal_skip_radius = goal_skip_radius_max;

		double nearest_obstacle = 1.0;

		double distance(std::vector<double> goal) {
			double x = pose.pose.pose.position.x - goal[0];
			double y = pose.pose.pose.position.y - goal[1];
			return sqrt(x*x + y*y);
		}

		double linear_vel(std::vector<double> goal) {

			double angle = scan.angle_min;

			// Velocity: the closer an object is to the front, the slower we want to go.
			double new_vel = 1.0;
			for (int i = 0; i < scan.ranges.size(); i++) {
				double p = scan.ranges[i]/scan.range_max-scan.range_min;

				// bias at front
				p += abs(angle) * 0.5;

				if (p < new_vel) { new_vel = p; }
				angle += scan.angle_increment;
			}

			// We want to make the goal skip radius smaller if obstacles are close.
			// This means we want to hug closer to the path.
			goal_skip_radius = goal_skip_radius_max * (std::pow(nearest_obstacle, 0.3)) + 0.1;

			std::cout << " GSK:" << goal_skip_radius << std::endl;

			// // Vroom!
			// if (find_angle(0) > 1.0) {
			// 	new_vel *= 2.0;
			// }

			return 4.22 * new_vel * distance(goal);
		}

		double angular_vel(std::vector<double> goal) {
			// https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
			double siny_cosp = 2.0 * (pose.pose.pose.orientation.w * pose.pose.pose.orientation.z + pose.pose.pose.orientation.x * pose.pose.pose.orientation.y);
			double cosy_cosp = 1.0 - 2.0 * (pose.pose.pose.orientation.y * pose.pose.pose.orientation.y + pose.pose.pose.orientation.z * pose.pose.pose.orientation.z);
			double theta = atan2(siny_cosp, cosy_cosp);

			if (theta < 0) {
				theta += 2*PI;
			} else if (theta > 2*PI) {
				theta = std::fmod(theta, 2*PI);
			}

			// std::cout << "a:" << ang << "t:" << theta << "z:" << pose.pose.pose.orientation.z << "w:" << pose.pose.pose.orientation.w << "\n";

			double goal_angle = angle(goal);

			if (goal_angle < 0) {
				goal_angle += 2*PI;
			} else if (goal_angle > 2*PI) {
				goal_angle = std::fmod(goal_angle, 2*PI);
			}

			// We want to compare goal_angle (-PI..PI) to our theta (-PI..PI)
			// But we want to remember that if we're at PI, and our goal is at -PI, we're actually facing the goal!
			double angle_delta_a = ( goal_angle - theta );
			double angle_delta_b = ( (goal_angle + 2*PI) - theta );
			double angle_delta_c = ( goal_angle - (theta + 2*PI) );

			// std::cout << "AA: " << angle_delta_a << " AB: " << angle_delta_b << " AC: " << angle_delta_c << std::endl;

			double angle_delta;
			if (abs(angle_delta_a) < abs(angle_delta_b) && abs(angle_delta_a) < abs(angle_delta_c)) {
				angle_delta = angle_delta_a;
			} else if (abs(angle_delta_b) < abs(angle_delta_a) && abs(angle_delta_b) < abs(angle_delta_c)) {
				angle_delta = angle_delta_b;
			} else {
				angle_delta = angle_delta_c;
			}

			// Create a bias in turning away from obstacles.
			double angle = scan.angle_min;
			double bias_angle = 0.0;

			// What is considered 'too close'.
			double cutoff = 0.35;

			for (int i = 0; i < scan.ranges.size(); i++) {
				double p = scan.ranges[i]/scan.range_max-scan.range_min;

				if (p < cutoff) {
					// Steer away!
					bias_angle += -angle;
				}

				angle += scan.angle_increment;
			}

			// Angles under this should be treated as 0, and we should focus on steering away from obstacles.
			double epsilon = 0.122;
			if (abs(angle_delta) < epsilon) {
				angle_delta = 0;
			}

			double base_angle_vel = 12.22 * (1.0 - nearest_obstacle) + 0.022;

			double angle_vel = base_angle_vel * angle_delta + 0.22 * bias_angle;

			std::cout << " AD:" << angle_delta << " BA:" << bias_angle << " NO:" << nearest_obstacle << std::endl;

			return angle_vel;
		}

		double angle(std::vector<double> goal) {
			return atan2(goal[1] - pose.pose.pose.position.y, goal[0] - pose.pose.pose.position.x);
		}

	public:

		nav_msgs::Odometry pose;
		sensor_msgs::LaserScan scan;
		geometry_msgs::Twist twist;

		// Returns true if we have reached the goal.
		bool drive_to(std::vector<double> goal) {
			double epsilon = 0.01;

			if (distance(goal) < epsilon) {
				std::cout << "Yay I'm close enough.\n";
				stop();
				return true;
			}

			twist.linear.x = linear_vel(goal);
			twist.angular.z = angular_vel(goal);
			return false;
		}

		void scan_update(sensor_msgs::LaserScan _scan) {
			scan = _scan;

			double angle = scan.angle_min;

			nearest_obstacle = 1.0;
			for (int i = 0; i < scan.ranges.size(); i++) {
				double p = scan.ranges[i]/scan.range_max-scan.range_min;

				if (p < nearest_obstacle) { nearest_obstacle = p; }
				angle += scan.angle_increment;
			}

			if (nearest_obstacle < 0) nearest_obstacle = 0;
		}

		void wander() {
			// Check our sensor readings.
			// We try to find the most free space in our sensor readings.

			double turn_speed = 0.5;
			double drive_speed = 2.0;

			// Angle: try to find the most free space.
			double new_angle = 0.0; // 0.0 is straight
			// Velocity: the closer any object is, the slower we want to go.
			double new_vel = 1.0;

			double angle = scan.angle_min;

			for (int i = 0; i < scan.ranges.size(); i++) {
				double p = scan.ranges[i]/scan.range_max-scan.range_min;
				new_angle += p * angle;
				if (p < new_vel) { new_vel = p; }
				angle += scan.angle_increment;
			}

			twist.angular.z = new_angle * turn_speed;
			twist.linear.x = new_vel * drive_speed;
		}

		int current_path_point = 0;

		std::deque<std::vector<double>>::iterator closest_next_path_point(std::deque<std::vector<double>> * path) {
			std::cout << "Looking up hard way..." << std::endl;

			double smallest_distance = 2222;

			int id = 0, smallest_path_point = 0;
			for (std::vector<double> path_point : *path) {
				double dist = distance(path_point);
				// Make sure we progress.
				if (true || id >= current_path_point) {
					if (dist < smallest_distance) {
						smallest_distance = dist;
						smallest_path_point = id;
					}
				}
				id++;
			}

			// std::cout << "SET CPP TO " << smallest_path_point << " FROM " << current_path_point << std::endl;
			current_path_point = smallest_path_point;

			std::deque<std::vector<double>>::iterator it = path->begin();

			// std::cout << "I am going " << current_path_point << " steps, and the list is " << path->size() << "long." << std::endl;

			std::advance(it, current_path_point);

			return it;
		}

		bool follow(std::deque<std::vector<double>> * path){
			// We want to find the closest point of the path to us.
			while (path->size() > 0) {
				std::deque<std::vector<double>>::iterator next_point = closest_next_path_point(path);

				// Skip points that are too close to us.
				while (distance(*next_point) < goal_skip_radius) {
					if ((*next_point)[0] == path->at(path->size()-1)[0]) {
						if ((*next_point)[1] == path->at(path->size()-1)[1]) {
							break;
						}
					}
					std::advance(next_point, 1);
				}

				// Go to point.
				if (drive_to(*next_point)) {
					path->pop_front();
				} else return false;
			}
			return true;
		}

		double find_angle(double angle_want) {
			double ranges = scan.ranges.size();

			if (angle_want < scan.angle_min || angle_want > scan.angle_max) {
				std::cout << "min: " << scan.angle_min << "max: " << scan.angle_max << "\n";
				throw std::runtime_error("Angle is not in range.");
			}

			double angle = scan.angle_min;

			for (int i = 0; i < ranges; i++) {
				if (angle < angle_want && angle_want < angle + scan.angle_increment) {
					return scan.ranges[i];
				}
				angle += scan.angle_increment;
			}

			throw std::runtime_error("Wrong.");
		}

		void stop() {
			twist.angular.z = 0;
			twist.linear.x = 0;
		}

		Driver() {
			twist.linear.x = twist.linear.y = twist.linear.z = 0;
			twist.angular.x = twist.angular.y = twist.angular.z = 0;
		}
};

Driver d;

void updatePoseCallback(const nav_msgs::Odometry::ConstPtr & msg) {
	d.pose = *msg;
}

void scan_callback(sensor_msgs::LaserScan scan) {
	d.scan_update(scan);
}

std::deque<std::vector<double>> read_points() {
	std::deque<std::vector<double>> path = { };

	visualization_msgs::Marker path_marker = *ros::topic::waitForMessage<visualization_msgs::Marker>("/path_planning");
	for (int i = 0; i < path_marker.points.size(); i++) {
		std::vector<double> point = { path_marker.points[i].x, path_marker.points[i].y };
		path.push_back(point);
	}

	return path;
}

int main(int argc, char ** argv){
	ros::init(argc, argv, "driver");

	ros::NodeHandle n;

	ros::Publisher pub = n.advertise<geometry_msgs::Twist>("/cmd_vel", 1000);
	// Use bpgt for now, TODO update to actual odom
	ros::Subscriber bpgt_sub = n.subscribe("/base_pose_ground_truth", 10, updatePoseCallback);

	ros::Subscriber scan_sub = n.subscribe("/noisy_base_scan", 1000, scan_callback);

	ros::Rate rate(100);

	std::cout << "Waiting for message...\n";

	// Wait until we have a path and a position.
	d.scan = *ros::topic::waitForMessage<sensor_msgs::LaserScan>("/noisy_base_scan");
	d.pose = *ros::topic::waitForMessage<nav_msgs::Odometry>("/base_pose_ground_truth");

	std::cout << "Message received!\n";

	while(ros::ok()) {
		std::deque<std::vector<double>> path = read_points();
		while(ros::ok()) {
			if (d.follow(&path)) break;
			// d.wander();
			pub.publish(d.twist);

			rate.sleep();
			ros::spinOnce();
		}
	}

	return 0;
}


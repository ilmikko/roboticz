/*
 * Mikko
 */

#include "ros/ros.h"
#include "std_msgs/String.h"
#include "nav_msgs/Odometry.h"
#include "visualization_msgs/Marker.h"
#include "tf/transform_broadcaster.h"

#include "png.h"
#include <queue>
#include <unordered_map>

png_bytep * row_pointers;
png_structp png_ptr;
png_infop info_ptr;

int png_width, png_height;

void load_map(char * fileName){
	//char header[8];

	FILE *fp = fopen(fileName, "rb");
	if (!fp) throw std::runtime_error("File could not be opened");
	//fread(header, 0, 8, fp);
	//if (!png_sig_cmp(header, 0, 8)) throw std::runtime_error("Header invalid");

	png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png_ptr) throw std::runtime_error("Create struct failed");
	info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr) throw std::runtime_error("Create info failed");

	if (setjmp(png_jmpbuf(png_ptr))) throw std::runtime_error("Error during init_io");

	// Read the PNG
	png_init_io(png_ptr, fp);
	// Read header
	png_set_sig_bytes(png_ptr, 0);
	// Read info
	png_read_info(png_ptr, info_ptr);

	// Get the info
	png_width = png_get_image_width(png_ptr, info_ptr);
	png_height = png_get_image_height(png_ptr, info_ptr);
	// color_type = png_get_color_type(png_ptr, info_ptr);
	// bit_depth = png_get_bit_depth(png_ptr, info_ptr);

	// number_of_passes = png_set_interlace_handling(png_ptr);
	png_read_update_info(png_ptr, info_ptr);

	// Read the file for data pointers
	if (setjmp(png_jmpbuf(png_ptr))) throw std::runtime_error("Error during read_image");

	row_pointers = (png_bytep*) malloc(sizeof(png_bytep) * png_height);
	for (int y = 0; y < png_height; y++) {
		row_pointers[y] = (png_byte*) malloc(png_get_rowbytes(png_ptr,info_ptr));
	}

	png_read_image(png_ptr, row_pointers);

	if (png_get_color_type(png_ptr, info_ptr) != PNG_COLOR_TYPE_RGB) throw std::range_error("Wrong color type (not RGB)");

	fclose(fp);
}

void save_map(char * fileName){
	FILE *fp = fopen(fileName, "wb");
	if (!fp) throw std::runtime_error("File could not be opened for writing");

	png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png_ptr) throw std::runtime_error("Create struct failed");
	info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr) throw std::runtime_error("Create info failed");

	png_init_io(png_ptr, fp);

	// Write header
	if (setjmp(png_jmpbuf(png_ptr))) throw std::runtime_error("Error writing header");

	png_set_IHDR(png_ptr, info_ptr, png_width, png_height, 8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

	png_write_info(png_ptr, info_ptr);

	if (setjmp(png_jmpbuf(png_ptr))) throw std::runtime_error("Error writing bytes");

	png_write_image(png_ptr, row_pointers);

	if (setjmp(png_jmpbuf(png_ptr))) throw std::runtime_error("Error writing end");

	png_write_end(png_ptr, NULL);
}

void debug_render_map(int w=100, int h=25){
	for (int y = 0; y < h; y++) {
		png_byte * row = row_pointers[y*(png_height/h)];
		for (int x = 0; x < w; x++) {
			png_byte * ptr = &(row[(x*3)*(png_width/w)]);
			int r = ptr[0];
			int g = ptr[1];
			int b = ptr[2];

			char const * str = ".";

			if (r+g+b < 255*1.5) str = "#";

			printf("[%d;%dH%s\n",y,x,str);
		}
	}
}

bool opaque(int x, int y){
	if (x < 0 || x >= png_width) {
		ROS_INFO("Your X-coordinate fucked up! (%i)", x);
		throw std::runtime_error("You fucked it up!");
	}
	if (y < 0 || y >= png_height) {
		ROS_INFO("Your Y-coordinate fucked up! (%i)", y);
		throw std::runtime_error("You fucked it up!");
	}
	png_byte * row = row_pointers[y];
	png_byte * ptr = &(row[x*3]);
	//ROS_INFO("Heres your probbie: %i,%i,%i",ptr[0],ptr[1],ptr[2]);
	if (ptr[0]+ptr[1]+ptr[2] < 255*1.5) return true;
	return false;
	//if (ptr[0]+ptr[1]+ptr[2] < 255*1.5) return false;
	//return true;
}

bool opaque_pixels(int xp, int yp, int size = 10){
	for (int y = std::max(yp - size, 0); y < std::min(yp + size, png_height); y++) {
		png_byte * row = row_pointers[y];
		for (int x = std::max(xp - size, 0); x < std::min(xp + size, png_width); x++) {
			if (opaque(x, y)) return true;
		}
	}
	return false;
}

void inflate_map(int size = 10){
	png_bytep * inflate_pointers;

	inflate_pointers = (png_bytep*) malloc(sizeof(png_bytep) * png_height);
	for (int y = 0; y < png_height; y++) {
		inflate_pointers[y] = (png_byte*) malloc(png_get_rowbytes(png_ptr, info_ptr));
	}

	for (int y = 0; y < png_height; y++) {
		png_byte * row = row_pointers[y];
		png_byte * inflate_row = inflate_pointers[y];

		for (int x = 0; x < png_width; x++) {
			png_byte * ptr = &(row[x*3]);
			png_byte * inflate_ptr = &(inflate_row[x*3]);

			int r = ptr[0];
			int g = ptr[1];
			int b = ptr[2];

			// Check if there are opaque pixels in this radius.
			if (opaque(x, y) || opaque_pixels(x, y, size)) {
				inflate_ptr[0] = 0;
				inflate_ptr[1] = 0;
				inflate_ptr[2] = 0;
			} else {
				inflate_ptr[0] = ptr[0];
				inflate_ptr[1] = ptr[1];
				inflate_ptr[2] = ptr[2];
			}
		}
	}

	row_pointers = inflate_pointers;
}

struct AstarNode {
	double x; // X
	double y; // Y
	double c; // Cost
	struct AstarNode * prev_node; // Pointer to a previous node
};

typedef struct AstarNode AstarNode;

double distance(double x1, double y1, double x2, double y2) {
	double dx = x1 - x2;
	double dy = y1 - y2;

	return std::sqrt(dx*dx + dy*dy);
}

std::deque<std::vector<double>> goals;

std::vector<std::list<std::vector<double>>> final_paths;

std::deque<int> winning_order;

class PathFinder {
	private:

		// Contains the list of coordinates we want to check through.
		//std::priority_queue<std::vector<double>, std::vector<std::vector<double>>> queue;
		// This contains the list of items that we have pushed into the queue.
		std::unordered_map<std::string, bool> checked;

		std::list<AstarNode*> getNeighbors(AstarNode * node) {
			int xp = node->x;
			int yp = node->y;
			double c = node->c;

			// ROS_INFO("Get NEIGH for %i,%i (c=%f)",xp,yp,c);

			std::list<AstarNode*> returns = { };

			for (int y = std::max(yp - 1, 0); y < std::min(yp + 2, png_height); y++) {
				png_byte * row = row_pointers[y];
				for (int x = std::max(xp - 1, 0); x < std::min(xp + 2, png_width); x++) {
					if (x == xp && y == yp) continue;

					// Do not include opaque cells.
					if (opaque(x, y)) continue;
					//ROS_INFO("Heres your probbie: %i",returns.size());

					// TODO: Use something more clever than a string conversion
					std::string neighbor_str = std::to_string(x)+","+std::to_string(y);

					// Do not include neighbors that are already checked.
					if (checked.find(neighbor_str) != checked.end()) continue;
					checked[neighbor_str] = true;

					AstarNode * neighbor_node = new AstarNode();

					neighbor_node->x = (double)x;
					neighbor_node->y = (double)y;
					neighbor_node->c = c + distance(xp, yp, x, y);
					neighbor_node->prev_node = node;

					returns.push_back(neighbor_node);
				}
			}

			return returns;
		}

		void debug_c_m(double x, double y){
			x = (x/png_width)*100;
			y = (y/png_height)*25;

			printf("[%i;%iH@\n", (int)y, (int)x);
		}

		void debug_c_p(double x, double y){
			x = (x/png_width)*100;
			y = (y/png_height)*25;

			printf("[%i;%iH!\n", (int)y, (int)x);
		}

	public:

		// Perform an A* search on our map to find a path from start to goal.
		std::list<std::vector<double>> find_path(std::vector<double> start, std::vector<double> goal){
			// Convert from ROS space to PNG space.
			start = rosToPNG(start);
			goal = rosToPNG(goal);

			checked.clear();

			// DEBUG: Show our start and end positions first.
			double sx = start[0], sy = start[1];
			double gx = goal[0], gy = goal[1];

			// debug_c_m(sx, sy);
			// debug_c_m(gx, gy);
			// END DEBUG

			// Comparison lambda.
			auto cmp = [&goal](AstarNode * left, AstarNode * right) {
				double cost1, cost2;

				// How far away are we from the target?
				cost1 = abs(left->x-goal[0]) + abs(left->y-goal[1]);
				cost2 = abs(right->x-goal[0]) + abs(right->y-goal[1]);

				// Add the travelled path as heuristic.
				double heur = 2.2;
				cost1 += left->c * heur;
				cost2 += right->c * heur;

				return cost1 > cost2;
			};

			// Contains the list of coordinates we want to check through.
			std::priority_queue<AstarNode*, std::vector<AstarNode*>, decltype(cmp)> queue(cmp);

			// This contains the list of items that we have pushed into the queue.
			checked = { };

			// Initialize cost as 0
			AstarNode * current = new AstarNode();
			current->x = start[0];
			current->y = start[1];
			current->c = 0;
			current->prev_node = NULL;

			// ROS_INFO("Start: %f,%f", start[0], start[1]);
			// ROS_INFO("Goal: %f,%f", goal[0], goal[1]);

			while (true) {
				//std::cout << "The address of the current node is: " << current << "\n";

				// Check if we have reached the goal.
				if ((int)current->x == (int)goal[0] && (int)current->y == (int)goal[1]) {
					// ROS_INFO("I have reached the goal!");
					break;
				}
				// Get valid neighbours of the current node.
				std::list<AstarNode*> neighbors = getNeighbors(current);

				for (AstarNode * n : neighbors) { 
					queue.push(n);
				}

				if (queue.empty()) {
					// We have failed.
					ROS_INFO("I have failed to find the goal, as I have ran out of things to check.");
					throw std::runtime_error("Cannot find path");
				}

				// Get the next node with the lowest cost.
				current = queue.top();
				queue.pop();

				//ROS_INFO("Size: %i", queue.size());
				//ROS_INFO("Current: %f,%f Goal: %f,%f", current[0], current[1], goal[0], goal[1]);
				// debug_c_m(current->x, current->y);
			}

			std::list<std::vector<double>> list = { };

			// Backtrack to find the path we took.
			AstarNode * node = current;
			while (node != NULL) {
				// std::cout << "vs: " << node << " :: " << node->x << "," << node->y << "\n";
				// debug_c_p(node->x, node->y);

				std::vector<double> point = { node->x, node->y };
				point = pngToRos(point);
				list.push_front(point);

				node = node->prev_node;
			}

			return list;
		}

		std::vector<double> rosToPNG(std::vector<double> coord){
			// TODO: Get these from the yaml file (or MAP METADATA)
			std::vector<double> origin = { -6.0, -4.8 };
			double resolution = 0.012;

			coord[0] = (coord[0] - origin[0]) / resolution;
			coord[1] = (coord[1] - origin[1]) / resolution;

			// Swap Y because of obvious reasons (TODO: elaborate on those)
			coord[1] = png_height - coord[1];

			return coord;
		}

		std::vector<double> pngToRos(std::vector<double> coord){
			// TODO: Get these from the yaml file (or MAP METADATA)
			std::vector<double> origin = { -6.0, -4.8 };
			double resolution = 0.012;

			// Swap Y because of obvious reasons (TODO: elaborate on those)
			coord[1] = png_height - coord[1];

			coord[0] = coord[0]*resolution + origin[0];
			coord[1] = coord[1]*resolution + origin[1];

			return coord;
		}

		void travelling_salesman() {
			// Euclidean traveling salesman.

			int n = goals.size();

			// Get 2-combinations without repetition.
			std::vector<int> v = { 0, 0, 0 };

			std::list<std::vector<int>> combinations;

			while(true) {
				for (int i = 0; i < 2; i++) {
					if (v[i] > n - 1) {
						v[i + 1] += 1;
						for (int k = i; k >= 0; k--) {
							v[k] = v[i + 1];
						}
					}
				}
				if (v[2] > 0) break;
				if (v[0] != v[1]) {
					std::vector<int> vec = { v[0], v[1] };
					combinations.push_back(vec);
				}
				v[0] += 1;
			}

			// Get distances between goals.
			// TODO: Calculate actual astar distances?
			std::map<std::vector<int>, double> distances;
			for (std::vector<int> vec : combinations) {
				std::vector<double> from = goals[vec[0]];
				std::vector<double> to = goals[vec[1]];

				// Store also the inverse of the path.
				std::vector<int> cev = { vec[1], vec[0] };

				distances[vec] =
					distances[cev] =
					distance(from[0], from[1], to[0], to[1]);

				// std::cout << "From " << from[0] << "," << from[1] << " to " << to[0] << "," << to[1] << " costs " << distances[vec] << std::endl;
			}

			// Brute force through all permutations.
			std::vector<int> p_goals;
			for (int i = 1; i < n; i++) p_goals.push_back(i);

			double min_distance = 2222;

			while (std::next_permutation(p_goals.begin(), p_goals.end())) {
				// goals is now arranged in one way.
				std::vector<int> start = { 0, p_goals[0] };
				double distance = distances[start];
				// double distance = 0;

				for (int i = 1; i < n; i++) {
					std::vector<int> path = { p_goals[i-1], p_goals[i] };
					distance += distances[path];
				}

				if (distance < min_distance) {
					min_distance = distance;
					winning_order.clear();
					winning_order.assign(p_goals.begin(), p_goals.end());
					winning_order.push_front(0);
				}
			}

			std::cout << "Winning path distance: " << min_distance << std::endl;
			for (int i = 1; i < n; i++) {
				std::vector<double> from = goals[winning_order[i-1]];
				std::vector<double> to = goals[winning_order[i]];

				// TODO: Calc path
				std::cout << "Path from " << (i - 1) << " til " << i << " ";
				std::cout << " (that's " << from[0] << "," << from[1] << " to " << to[0] << "," << to[1] << ")";
				std::list<std::vector<double>> path = find_path(from, to);
				std::cout << " :D " << std::endl;

				final_paths.push_back(path);
			}
		}

};

void cleanup_png(){
	for (int y = 0; y < png_height; y++) {
		free(row_pointers[y]);
	}

	free(row_pointers);
}

int current_goal = 0;

visualization_msgs::Marker path_marker(std::list<std::vector<double>> path){
	// Display path marker.
	visualization_msgs::Marker marker;
	marker.header.frame_id = "map";
	marker.header.stamp = ros::Time::now();

	marker.id = 0;
	marker.type = visualization_msgs::Marker::LINE_STRIP;
	marker.action = visualization_msgs::Marker::ADD;

	for (std::vector<double> n : path) {
		geometry_msgs::Point p;

		p.x = n[0];
		p.y = n[1];
		p.z = 0;

		marker.points.push_back(p);
	}

	// Line width.
	marker.scale.x = 0.02;
	marker.scale.y = 0.02;
	marker.scale.z = 0.02;

	marker.color.a = 1;
	marker.color.r = 1;
	marker.color.g = 0;
	marker.color.b = 0;

	return marker;
}

visualization_msgs::Marker goalMarker(std::vector<double> pos){
	// Display goal marker.
	visualization_msgs::Marker marker;
	marker.header.frame_id = "map";
	marker.header.stamp = ros::Time::now();

	marker.id = 0;
	marker.type = visualization_msgs::Marker::CYLINDER;
	marker.action = visualization_msgs::Marker::ADD;

	marker.pose.position.x = pos[0];
	marker.pose.position.y = pos[1];
	marker.pose.position.z = 0;

	marker.scale.x = 0.2;
	marker.scale.y = 0.2;
	marker.scale.z = 0.2;

	marker.color.a = 1;
	marker.color.r = 1;
	marker.color.g = 0;
	marker.color.b = 0;

	return marker;
}

void updateGoalCallback(const nav_msgs::Odometry msg) {
	double rx = msg.pose.pose.position.x;
	double ry = msg.pose.pose.position.y;

	std::vector<double> goal = goals[winning_order[current_goal + 1]];

	double gx = goal[0];
	double gy = goal[1];

	double dist = distance(rx, ry, gx, gy);

	if (dist < 0.3) {
		std::cout << "Goal reached!!!" << std::endl;
		current_goal += 1;
	}
}

int main(int argc, char **argv){
	if (argc < 2) {
		throw std::runtime_error("Not enough arguments!");
	}
	// if (argc > 1) {
	// 	load_map(argv[1]);
	// 	inflate_map(6);
	// 	// debug_render_map();
	// 	save_map("assets/map_inflated.png");
	// 	return 0;
	// }

	ros::init(argc, argv, "planning");
	ros::NodeHandle n;

	ros::Publisher path_plan_pub = n.advertise<visualization_msgs::Marker>("/path_planning", 1000);
	ros::Subscriber bpgt = n.subscribe<nav_msgs::Odometry>("/base_pose_ground_truth", 10, updateGoalCallback);

	visualization_msgs::Marker goalMarkers [5];
	ros::Publisher goalPubs [5];

	goals = {};
	for (int i = 0; i < 5; i++) {
		if (ros::param::has("/goal" + std::to_string(i))) {
			std::vector<double> goal;
			ros::param::get("/goal" + std::to_string(i), goal);
			goals.push_back(goal);

			goalPubs[i] = n.advertise<visualization_msgs::Marker>("/goal" + std::to_string(i) + "_marker", 1000);
			goalMarkers[i] = goalMarker(goal);
		} else {
			std::cerr << "Error: Parameter not found for /goal" << i << std::endl;
			return 1;
		}
	}

	std::vector<double> star;
	ros::param::get("/robot_start", star);
	std::vector<double> start = { star[0], star[1] };
	goals.push_front(start);

	PathFinder p;

	load_map(argv[1]);
	// debug_render_map();

	p.travelling_salesman();

	// std::list<std::vector<double>> path = p.find_path(start, goal);
	ros::Rate rate(100);

	while(ros::ok()){
		for (int i = 0; i < 5; i++) {
			goalPubs[i].publish(goalMarkers[i]);
		}

		path_plan_pub.publish(path_marker(final_paths[current_goal]));

		ros::spinOnce();

		rate.sleep();
	}

	cleanup_png();

	return 0;
}

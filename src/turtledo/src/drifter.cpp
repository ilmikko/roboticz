/**
 * driver.cpp
 * One line description about this file
 * More information
 */

#include "ros/ros.h"
#include <cstdio>
#include <geometry_msgs/Twist.h>

#include <turtlesim/Pose.h>

class Driver {
	public:

	geometry_msgs::Twist twist;
	turtlesim::Pose pose;

	double distance(std::vector<double> goal) {
		double x = pose.x - goal[0];
		double y = pose.y - goal[1];

		x *= x;
		y *= y;

		return sqrt(x + y);
	}

	double linear_vel(std::vector<double> goal) {
		double c = 1.5;
		return c * distance(goal);
	}

	double angular_vel(std::vector<double> goal) {
		double c = 6;
		return c * (angle(goal) - pose.theta);
	}

	double angle(std::vector<double> goal) {
		return atan2(goal[1] - pose.y, goal[0] - pose.x);
	}

	// Returns true if we have reached the goal.
	bool drive_to(std::vector<double> goal) {
		double epsilon = 0.01;
		if (distance(goal) < epsilon) {
			stop();
			return true;
		}

		twist.linear.x = linear_vel(goal);
		twist.angular.z = angular_vel(goal);
		return false;
	}

	void stop() {
		twist.angular.z = 0;
		twist.linear.x = 0;
	}

	Driver() {
		twist.linear.x = 0;
		twist.angular.z = 0;
	}
};

Driver d;

void updatePoseCallback(const turtlesim::PoseConstPtr &msg) {
	d.pose = *msg;
}

int main(int argc, char ** argv){
	ros::init(argc, argv, "driver");

	ros::NodeHandle n;

	ros::Publisher pub = n.advertise<geometry_msgs::Twist>("turtle1/cmd_vel", 1000);
	ros::Subscriber sub = n.subscribe("turtle1/pose", 1, updatePoseCallback);
	ros::Rate rate(2);

	std::vector<double> goal = { 10, 10 };

	if (argc == 3) {
		std::cout << "Reading goal from cli args.";
		goal[0] = atoi(argv[1]);
		goal[1] = atoi(argv[2]);
	}

	while(ros::ok()) {
		if (d.drive_to(goal)) break;
		pub.publish(d.twist);

		rate.sleep();
		ros::spinOnce();
	}

	return 0;
}

